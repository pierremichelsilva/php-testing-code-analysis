<?php namespace Calculator;

/**
 * Description of Calculator
 *
 * @author pierre
 */
class Calculator {
    
    /**
     * Suma dos numeros...
     * @param type $a
     * @param type $b
     * @return type
     * @assert (0, 0) == 0
     * @assert (0, 1) == 1
     * @assert (1, 0) == 1
     * @assert (1, 1) == 2
     * @assert (1, 2) == 3
     */
    public function add($a, $b)
    {
        return $a + $b;
    }
    
    /**
     * 
     * @param type $a
     * @param type $b
     * @return type
     * @assert (1, 0) == 1
     * @assert (1, 1) == 0
     * @assert (0, -2) == 2
     * @assert (2, 1) == 2
     */
    public function substrac($a, $b) {
        return $a - $b;
    }
    
    
    /**
     * Divide 2 numeros
     * @param type $a
     * @param type $b
     * @return int
     * 
     * @assert (1, 1) == 1
     * @assert (2, 3) == 3
     */
    public function division($a,$b) {
        
        if($b == 0)
        {
            return 0;
        }
        
        return $a / $b;
    }    
    
    /**
     * Multiplica $a * $b
     * @param type $a
     * @param type $b
     * @return type
     * 
     * @assert (1, 2) == 2
     * @assert (2, 3) == 6
     */
    public function multiplication($a,$b) {
        return $a * $b;
    }
}
